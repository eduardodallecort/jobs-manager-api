package br.edu.unisep.jobsmanager.controller;

import br.edu.unisep.jobsmanager.domain.dto.company.CompanyDto;
import br.edu.unisep.jobsmanager.domain.dto.company.NewCompanyDto;
import br.edu.unisep.jobsmanager.domain.usecase.company.FindAllCompaniesUseCase;
import br.edu.unisep.jobsmanager.domain.usecase.company.FindCompaniesByIndustryUseCase;
import br.edu.unisep.jobsmanager.domain.usecase.company.SaveCompanyUseCase;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/company")
public class CompanyController {

    private FindAllCompaniesUseCase findAllUseCase;
    private SaveCompanyUseCase saveUseCase;
    private FindCompaniesByIndustryUseCase findByIndustry;

    @GetMapping
    public ResponseEntity<List<CompanyDto>> findAll() {
        List<CompanyDto> companies = findAllUseCase.execute();

        if (companies.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(companies);
    }

    @GetMapping("/byIndustry/{industry}")
    public ResponseEntity<List<CompanyDto>> findByIndustry(@PathVariable Integer industry) {
        List<CompanyDto> companies = findByIndustry.execute(industry);

        if (companies.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(companies);
    }

    @PostMapping
    public ResponseEntity<Object> save(@RequestBody NewCompanyDto newCompany) {
        saveUseCase.execute(newCompany);
        return ResponseEntity.ok().build();
    }
}
