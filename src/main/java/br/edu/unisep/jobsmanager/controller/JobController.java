package br.edu.unisep.jobsmanager.controller;

import br.edu.unisep.jobsmanager.domain.dto.job.JobDto;
import br.edu.unisep.jobsmanager.domain.dto.job.NewJobDto;
import br.edu.unisep.jobsmanager.domain.usecase.job.FindJobByCompanyUseCase;
import br.edu.unisep.jobsmanager.domain.usecase.job.FindJobByIndustryUseCase;
import br.edu.unisep.jobsmanager.domain.usecase.job.SaveJobUseCase;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/job")
public class JobController {

    private FindJobByCompanyUseCase findByCompanyUseCase;
    private FindJobByIndustryUseCase findByIndustryUseCase;
    private SaveJobUseCase saveUseCase;

    @GetMapping("/byCompany/{company}")
    public ResponseEntity<List<JobDto>> findByCompany(@PathVariable Integer company) {
        List<JobDto> jobs = findByCompanyUseCase.execute(company);

        if (jobs.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(jobs);
    }

    @GetMapping("/byIndustry/{industry}")
    public ResponseEntity<List<JobDto>> findByIndustry(@PathVariable Integer industry) {
        List<JobDto> jobs = findByIndustryUseCase.execute(industry);

        if (jobs.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(jobs);
    }

    @PostMapping
    public ResponseEntity<Object> save(@RequestBody NewJobDto newJob) {
        saveUseCase.execute(newJob);
        return ResponseEntity.ok().build();
    }

}
