package br.edu.unisep.jobsmanager.domain.dto.company;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewCompanyDto {

    private String name;
    private Integer industry;
}
