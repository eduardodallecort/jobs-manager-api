package br.edu.unisep.jobsmanager.domain.usecase.industry;

import br.edu.unisep.jobsmanager.data.entity.Industry;
import br.edu.unisep.jobsmanager.data.repository.IndustryRepository;
import br.edu.unisep.jobsmanager.domain.dto.industry.IndustryDto;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class FindAllIndustriesUseCase {

    private IndustryRepository repository;

    public List<IndustryDto> execute() {
        List<Industry> industries = repository.findAll(Sort.by("name"));
        return industries.stream().map(i -> new IndustryDto(i.getId(), i.getName()))
                .collect(Collectors.toList());
    }

}
