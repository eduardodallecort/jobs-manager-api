package br.edu.unisep.jobsmanager.domain.usecase.job;

import br.edu.unisep.jobsmanager.data.entity.Job;
import br.edu.unisep.jobsmanager.data.repository.JobRepository;
import br.edu.unisep.jobsmanager.domain.dto.job.JobDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class FindJobByIndustryUseCase {

    private JobRepository repository;

    public List<JobDto> execute(Integer industry) {
        List<Job> jobs = repository.findByIndustry(industry);

        return jobs.stream().map(j ->
                new JobDto(j.getId(), j.getTitle(), j.getDescription(), j.getSeniority(), j.getSalary(), j.getCompany().getName())
        ).collect(Collectors.toList());
    }
}
