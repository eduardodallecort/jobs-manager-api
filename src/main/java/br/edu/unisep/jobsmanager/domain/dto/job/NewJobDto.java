package br.edu.unisep.jobsmanager.domain.dto.job;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewJobDto {

    private String title;
    private String description;
    private String seniority;

    private Double salary;

    private Integer company;

}
