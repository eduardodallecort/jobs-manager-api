package br.edu.unisep.jobsmanager.domain.dto.industry;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class IndustryDto {

    private Integer id;
    private String name;
    
}
