package br.edu.unisep.jobsmanager.domain.usecase.company;

import br.edu.unisep.jobsmanager.data.entity.Company;
import br.edu.unisep.jobsmanager.data.repository.CompanyRepository;
import br.edu.unisep.jobsmanager.domain.dto.company.CompanyDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class FindAllCompaniesUseCase {

    private CompanyRepository repository;

    public List<CompanyDto> execute() {
        List<Company> companies = repository.findAll();

        return companies.stream().map(c ->
                new CompanyDto(c.getId(), c.getName(), c.getIndustry().getName())
        ).collect(Collectors.toList());
    }
}
