package br.edu.unisep.jobsmanager.domain.usecase.job;

import br.edu.unisep.jobsmanager.data.entity.Company;
import br.edu.unisep.jobsmanager.data.entity.Job;
import br.edu.unisep.jobsmanager.data.repository.JobRepository;
import br.edu.unisep.jobsmanager.domain.dto.job.NewJobDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class SaveJobUseCase {

    private JobRepository repository;

    public void execute(NewJobDto newJob) {
        Job job = new Job();
        job.setSalary(newJob.getSalary());
        job.setSeniority(newJob.getSeniority());
        job.setDescription(newJob.getDescription());
        job.setTitle(newJob.getTitle());

        Company company = new Company();
        company.setId(newJob.getCompany());

        job.setCompany(company);

        repository.save(job);
    }

}
