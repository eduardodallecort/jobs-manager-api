package br.edu.unisep.jobsmanager.domain.dto.company;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class CompanyDto {

    private Integer id;
    private String name;
    private String industry;
}
