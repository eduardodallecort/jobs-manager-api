package br.edu.unisep.jobsmanager.domain.usecase.company;

import br.edu.unisep.jobsmanager.data.entity.Company;
import br.edu.unisep.jobsmanager.data.entity.Industry;
import br.edu.unisep.jobsmanager.data.repository.CompanyRepository;
import br.edu.unisep.jobsmanager.domain.dto.company.NewCompanyDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class SaveCompanyUseCase {

    private CompanyRepository repository;

    public void execute(NewCompanyDto newCompany) {
        Company company = new Company();
        company.setName(newCompany.getName());
        company.setIndustry(new Industry());
        company.getIndustry().setId(newCompany.getIndustry());

        repository.save(company);
    }
}
