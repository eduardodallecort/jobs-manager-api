package br.edu.unisep.jobsmanager.domain.dto.job;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JobDto {

    private Integer id;

    private String title;
    private String description;
    private String seniority;

    private Double salary;

    private String company;

}
