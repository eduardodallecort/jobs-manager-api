package br.edu.unisep.jobsmanager.data.repository;

import br.edu.unisep.jobsmanager.data.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Integer> {

    List<Company> findByIndustry_IdOrderByName(Integer industry);

}
