package br.edu.unisep.jobsmanager.data.repository;

import br.edu.unisep.jobsmanager.data.entity.Job;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobRepository extends JpaRepository<Job, Integer> {

    @Query("from Job where company.id = :company")
    List<Job> findByCompany(@Param("company") Integer company);

    @Query("from Job where company.industry.id = :industry")
    List<Job> findByIndustry(@Param("industry") Integer industry);

}
